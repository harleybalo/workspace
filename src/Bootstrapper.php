<?php
namespace App;

class Bootstrapper
{
     /**
     *  Current URL
     *  @return  string
     */
    public function getCurrentUrl()
    {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    /**
     *  Base URL
     *  @return  string
     */
    public function getBaseUrl()
    {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"];
        }
        return $pageURL;
    }

    /**
     *  Current URL
     *   - Loads application via controllers
     *  @return  mixed
     */
    public function handle()
    {
        $url = $this->getCurrentUrl();
        $parse_url = parse_url($url);   
        $page_found = false;

        if (isset($parse_url['path'])) {
            $paths  = $parse_url['path'];
            $class  = 'App';
            $method = 'index';
            $id     = false;

            if ($paths !== '/') {
                $path   = explode('/', trim($paths, '/'));
                $class  = isset($path[0]) ? ucfirst($path[0]) : false;
                $method = isset($path[1]) ? $path[1] : "index";
                $id     = isset($path[2]) ? $path[2] : false;
            }
                
            if ($class) {
                $class = 'App\\Controllers\\' . $class . 'Controller';
                if (class_exists($class) ) {
                    $method = $method . 'Action';
                    if (method_exists($class, $method)) {
                        $obj = new $class();
                        return $obj->$method($id);
                    }
                }
            }
            header("HTTP/1.0 404 Not Found");
            echo ('Page not found');
        }
    }
}