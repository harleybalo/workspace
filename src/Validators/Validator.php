<?php

namespace App\Validators;


abstract class Validator 
{

    /**
     * Errors
     *
     * @var array
     */
    protected $errors = [];
    
    /**
     * Validation rules
     *
     * @return array
     */
    public abstract function rules();

    /**
     * Validate
     *
     * @param array
     *
     * @return Validator
     */
    public function validate(array $data)
    {
        $rules = $this->rules();
        foreach ($rules as $field => $requirement) {
            $requirements = explode('|', $requirement);

            $_field = str_replace(['_', '-'], ' ', $field);
            $_field = strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $_field));
            $value  = isset($data[$field]) ? $data[$field] : false;
            foreach ($requirements as $key => $_req) {
                if (preg_match('/(trim)/', $_req) ) {
                    $value = !is_array($value) ? trim($value) : $value;
                }
                
                switch ($_req) {
                    case 'required':
                        if ( !$value ) {
                            $this->errors[$field] = "The " . $_field . " field is required";
                        } 
                        break;
                    case ( preg_match('/(maxlength:)/', $_req) != false ) :
                        $_len = filter_var($_req, FILTER_SANITIZE_NUMBER_INT);
                        $len  = strlen($value);
                        if ($len > $_len) {
                            $this->errors[$field] = "The " . $_field . " field allows max  {$_len} characters";
                        }
                        break;
                    case ( preg_match('/(minlength:)/', $_req) != false ) :
                        $_len = filter_var($_req, FILTER_SANITIZE_NUMBER_INT);
                        $len  = strlen($value);
                        if ($len < $_len) {
                            $this->errors[$field] = "The " . $_field . " field is requires minimum of {$_len} characters";
                        }
                        break;
                    case 'valid_past_date':
                        if ($value) {
                            $valid = false;
                            $dates = explode("-", $value);
                            if (count($dates) === 3 ) {
                                list($year, $month, $day) = $dates;
                                if (checkdate($month, $day, $year) && strtotime($value) < strtotime('today')) {
                                    $valid = true;
                                }
                            }
                            if (!$valid) {
                                $this->errors[$field] = "The " . $_field . " field requires a valid date in the past";
                            }
                        } 
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }

        return $this;
    }

    /**
     * Passed
     *
     * @var bool
     */
    public function passed()
    {
        return !$this->errors ?: false;
    }

    /**
     * Errors
     *
     * @return array
     */
    public  function errors()
    {
        return $this->errors;
    }
}