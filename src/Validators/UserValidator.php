<?php
namespace App\Validators;

use App\Validators\Validator;

class UserValidator extends Validator
{
     /**
     * Rules
     * @var  array
     */
    protected $rules = [];

    public function rules()
    {
    	$year = date("Y");
        return $this->rules = [
            'date' => 'required|valid_past_date',
            'name'  => 'required|minlength:2|maxlenght:50',
        ];
    }
}