<?php
namespace App\Entity;

use App\DateConverter;

/**
 * @Entity @Table(name="users")
 *
 */
class Users {
    
    /** @Id @Column(type="integer", name="id") @GeneratedValue **/
    protected $id;

    /** @Column(type="string", name="name") **/
    protected $name;

    /** @Column(type="datetime", name="date_of_birth") **/
    protected $dateOfBirth;

    /** @Column(type="datetime", name="created_at") **/
    protected $createdAt;

    

    public function getId()
    {
        return $this->id;
    }

     /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;
    }



    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return Users
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Users
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get DOB in hours
     *
     * @return string
     */
    public function getDOBInHours()
    {
        return DateConverter::diffInHours($this->dateOfBirth);
    }

    /**
     * Get DOB in days
     *
     * @return string
     */
    public function getDOBInDays()
    {
        return DateConverter::diffInDays($this->dateOfBirth);
    }

    /**
     * Get DOB in days
     *
     * @return string
     */
    public function getDOBInYears()
    {
        return DateConverter::diffInYears($this->dateOfBirth);
    }
}