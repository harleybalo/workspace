<?php
namespace App\Engine;
use App\Engine\EntityManager;
use App\Engine\Redirect;
/**
* 
*/
class BaseController
{
    use Redirect;
     /**
     * $error
     * @var array
     */
    public $error;

    /**
     * $vars
     * @var array
     */
    public $vars = array();

     /**
     * Entity Manager available in the controller
     * @return  EntityManager
     */
	public function getEntityManager()
	{
		return (new EntityManager())->manager();
	}

     /**
     *  View Display
     *  - Set all data and load the template required
     * @return  mixed
     */
    protected function view($_view, $_data = false)
    {
        if ($_data && is_array($_data)) {
            $this->vars = array_merge($_data, $this->vars);
        }

        foreach ($this->vars as $_k => $_val){
            $$_k = $_val;
        }

        if (!isset($this->vars['errors'])) {
            $errors = array();
        }

        $error = $this->error;       
        $body = "error/page_not_found";

        if ($_view) {
            $_file = dirname(__DIR__) . '/Resources/views/body/' . $_view . '.php';
            if (file_exists($_file)) {
                $body = $_file;
            }
        }
            
        require_once dirname(__DIR__) . '/Resources/views/base.php';
        unset($_SESSION['flash_messages'], $_SESSION['form_data']);
    }
}