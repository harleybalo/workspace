<?php
namespace App\Engine;

trait Redirect {

    public function withData($data = array()) 
    {
        $_SESSION['form_data'] = array();

        if ($data) {
            foreach ($data as $key => $value) {
               $_SESSION['form_data'][$key] = $value;
            }
        }

        return $this;
    }

    public function addFlash($message, $type = 'error') 
    {
        if ($message) {
            if (is_array($message)) {
                foreach ($message as $field => $_message) {
                    $_SESSION['flash_messages'][$type][$field] =  $_message;
                }
            } else {
                $_SESSION['flash_messages'][$type] =  $message;
            }
        }
        return $this;
    }

    public function redirect($page)
    {
        header('Location: ' . base_url() . $page);
        exit();
    }

    public function redirectWithSuccess($page, $message)
    {
        $this->addFlash($message, 'success');
        return $this->redirect($page);
    }

    public function redirectWithError($page, $message)
    {
        $this->addFlash($message, 'error');
        return $this->redirect($page);
    }

    public function redirectWithErrors($page, $errors, $data = array())
    {
        $this->addFlash($errors, 'error');
        $this->withData($data);
        return $this->redirect($page);
    }
}