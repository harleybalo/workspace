<?php 

namespace App\Engine;

use App\Engine\Connection;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager as Manager;

class EntityManager {

    private static $_instance; 

	public static function manager()
	{
        if (self::$_instance) {
            echo "Instanced";
            return self::$_instance;
        }

		$paths = [
            dirname(__DIR__) ."/Entity",
        ];

        $isDevMode = true;

        $conn = array(
            'driver' => 'pdo_sqlite',
            'path'   => dirname(__DIR__).'/database/db.sqlite',
        );

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        return self::$_instance =  Manager::create($conn, $config);
	}
}