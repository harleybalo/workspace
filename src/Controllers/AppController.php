<?php
namespace App\Controllers;
use App\Engine\BaseController as Controller;
use App\Listeners\UserPostInfoListener as User;

/**
*  App Controller
*/
class AppController extends Controller
{
	public function __construct()
	{
		$this->vars['post'] = [
			'name'	=> false,
			'date'	=> false,
			'yesterday' => date("Y-m-d", strtotime( '-1 days' ) ),
		];
	}

	 /**
     *  Landing Page
     *  - Lists the first x number of data 
     *	- If all param is set, list all data
     *
     * @return  mixed
     */
	public function indexAction()
	{
		$em  = $this->getEntityManager();
		$max = isset($_GET['all']) ? null : 25;
        $this->vars['users'] = $em->getRepository("App\Entity\Users")->findBy([], ['id' => 'DESC'], $max, 0);

        if (isset($_SESSION['form_data'])) {
        	$this->vars['post'] = array_merge($this->vars['post'], $_SESSION['form_data']);
        }

        $this->view('home');
	}

	 /**
     * Post data
     *  - On post, call the listener to register request
     *	- If ajax, returns json else redirects
     * @return  mixed
     */
	public function postAction()
	{
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    	$response = User::handle($post);
		
		if( (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || isset($_GET['callback']) ) {
			echo json_encode($response);
		} else {
			if ($response['errors']) {
				return $this->redirectWithErrors('app', $response['errors'], $post);
			} else {
				return $this->redirectWithSuccess('app', 'Date of birth successfully added');
			}
		}
	}
}

