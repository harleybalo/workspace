<div class="container">
    <?php if ($users) : ?>
        <div class="user-listings">
            <div class="row">  
                <div class="col-sm-5">
                    <div class="user-form">
                        <form class="form" method="post" action="/app/post" onsubmit="return submitForm(this);">
                            <div class="form-errors">
                                <?php if ($errors): ?>
                                    <p> Please correct the errors</p>
                                    <ul>
                                        <?php foreach ($errors as $key => $error) : ?>
                                            <li><?php echo $error; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <ul class="errors-ul">
                                    
                                </ul>
                            </div>
                            <div class="form-success">
                                <?php if (isset($_SESSION['flash_messages']['success'])) : ?>
                                    <?php echo $_SESSION['flash_messages']['success']; ?>
                                    <?php unset($_SESSION['flash_messages']['success']); ?>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control name" id="name" maxlength="50" placeholder="Name" value="<?php echo $post['name']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="date" name="date" class="form-control date" id="date" maxlength="50" placeholder="date" value="<?php echo $post['date']; ?>" max="<?=$post['yesterday']; ?>">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-7"> 
                    <?php if (!isset($_GET['all'])) : ?>
                        <div class="alert alert-info">
                            Showing last 5 registrations <a href="/app?all" class="show-all">show all</a>
                        </div>
                    <?php endif; ?>
                    <div class="table table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>DOB</th>
                                    <th>Hours</th>
                                    <th>Days</th>
                                    <th>Years</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $key => $user): ?>
                                    <tr>
                                        <td><?php echo $user->getName(); ?></td>
                                        <td><?php echo $user->getDateOfBirth()->format('Y-m-d'); ?></td>
                                        <td><?php echo $user->getDOBInHours(); ?> hours</td>
                                        <td><?php echo $user->getDOBInDays(); ?> days</td>
                                        <td><?php echo $user->getDOBInYears(); ?> years</td>
                                    </tr>   
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="alert alert-warning">No data available</div>
    <?php endif; ?>
</div>
