<?php 
namespace App;

use Carbon\Carbon;

class DateConverter {

	public static function diffInHours(\Datetime $date)
	{
		$dtDate = self::createFromFormat($date);
		return Carbon::now()->diffInHours($dtDate);
	}

	public static function diffInDays(\Datetime $date)
	{
		$dtDate = self::createFromFormat($date);
		return Carbon::now()->diffInDays($dtDate);
	}

	public static function diffInYears(\Datetime $date)
	{
		$dtDate = self::createFromFormat($date);
		return Carbon::now()->diffInYears($dtDate);
	}

	public static function createFromFormat(\Datetime $datatime) 
	{
		$date = $datatime->format('Y-m-d');
		return Carbon::createFromFormat('Y-m-d', $date);
	}
}