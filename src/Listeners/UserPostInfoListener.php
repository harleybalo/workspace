<?php
namespace App\Listeners;
use App\Validators\UserValidator;
use App\Entity\Users;
use App\Engine\EntityManager;

class UserPostInfoListener
{
	 /**
     *  Attempt to register request
     *  - Errors are returned from validation if any
     * @return  array
     */
	public static function handle($data)
	{
		$validator = (new UserValidator())->validate($data);
		if ($validator->passed()) {
			$user = new Users;
			$dob  = $data['date'];
			$name = $data['name'];
			$user->setDateOfBirth(new \DateTime($dob));
			$user->setName($name);
			$user->setCreatedAt(new \DateTime('Europe/London'));
			$em = EntityManager::manager();
			$em->persist($user);
			$em->flush($user);

			$userData = [
				'dob' 	=> $dob,
				'years' => $user->getDOBInYears(),
				'hours' => $user->getDOBInHours(),
				'days' 	=> $user->getDOBInDays(),
				'name'	=> $name,
			];

			return [
				'data'	=> $userData,
				'errors' => false,
			];
		}
		
		return [
			'errors' => $validator->errors(),
		];
	}
}