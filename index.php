<?php
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Europe/London');

require __DIR__ . '/vendor/autoload.php';
use App\Bootstrapper;


/**
 * @author Adekunle Balogun
 * @package test
 *
 */
class Bootstrap
{
    public static function main()
    {
        (new Bootstrapper())->handle();
    }

}

function base_url() {
	return (new Bootstrapper())->getBaseUrl();
}

Bootstrap::main();

