<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require __DIR__ . '/vendor/autoload.php';


$paths = array(__DIR__ ."/src/Entity");
$isDevMode = true;

$conn = array(
	'driver' => 'pdo_sqlite',
    'path' 	 => __DIR__.'/src/database/db.sqlite',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($conn, $config);

$users = new App\Entity\Users;
$users->setCreatedAt("2017-01-20 00:00:00");
$users->setDateOfBirth("2017-01-20 00:00:00");
$entityManager->persist($users);
$entityManager->flush();