var submitForm = function(e) {
        jQuery('.errors-ul').empty();
        jQuery('.alert-success').remove();
        var fields = e.getElementsByTagName('input');
        var dt = new Date();
        var thisYear = dt.getFullYear();
        for (var i = fields.length - 1; i >= 0; i--) {
            var a = fields[i];
            var name = a.getAttribute('name');
            var value = a.value;
            switch(name) {
                case 'name':
                    if (value.length < 2 || value > 50) {
                        jQuery('.errors-ul').prepend('<li>Name field allows only between 2 and 50 characters');
                    }
                    var fullname = value;
                    break;
                case 'date':
                    if (!value.length) {
                        jQuery('.errors-ul').prepend('<li>A valid date is required');
                    }
                    var date = value;
                    break;
            }         
        }

        if (!jQuery('.errors-ul li').length) {
            var data = {
                name: fullname,
                date: date,
            }
            postData(data);
        }
        
        return false;
    }

    var postData = function(data) {
        jQuery.post('/app/post?callback=?', data)
        .done(function(res) {
            var res = jQuery.parseJSON(res);
            if (res.errors) {
                var err = res.errors;
                jQuery.each(err, function(i, v) {
                   jQuery('.errors-ul').prepend('<li> ' + v + ' </li>');
                })
            } else {
                var dob = res.data;
                var tr = "<tr><td>" + dob.name + "</td><td>" + dob.dob + "</td><td>" + dob.hours + " hours</td><td>" + dob.days + " days</td><td>" + dob.years + " years</td></tr>";
                jQuery('.table tbody').prepend(tr);
                jQuery('.user-listings').prepend("<div class='alert alert-success'>Request was successful</div>");
                jQuery('#name, #date').val("");
            }
        })
        .always(function(res) {
        })
        .fail(function(res) {
        });
    }

    var isDateValid = function (year, month, day) {
        return true;
        var d = new Date(year, month, day);
        if (d.getFullYear() == year && d.getMonth() == month && d.getDay() == day) {
            return true;
        }
        return false;
    }